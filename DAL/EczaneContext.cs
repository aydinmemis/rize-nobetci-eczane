﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
   public class EczaneContext:DbContext
    {
        public EczaneContext():
            base("EczaneContext")
        {

        }
        public virtual DbSet<Eczane> Eczane { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
