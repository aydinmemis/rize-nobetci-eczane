﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    [Table("Eczane")]
    public  class Eczane
    {
        [Key]
        public int Id { get; set; }
        public string Ilce { get; set; }
        public string EczaneAd { get; set; }
        public string Adres { get; set; }
        public string Telefon { get; set; }

      //  public DateTime CreatedDate { get; set; }
    }
}
