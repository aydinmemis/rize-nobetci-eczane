﻿using HtmlAgilityPack;
using HtmlParse.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;


namespace HtmlParse.Controllers
{
    public class HomeController : Controller
    {
        private string html;

        // GET: Home
        public ActionResult Index()
        {
            List<Eczane> datas;

            var url = "http://www.rsm.gov.tr/nobetcieczaneler.html";
            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;

            try
            {
                html = client.DownloadString(url);
            }
            catch (Exception)
            {

                throw;
            }


            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(html);

            var table = doc.DocumentNode
           .Descendants("tr")
           .Select(n => n.Elements("td").Select(e => e.InnerText).ToList());

            datas = new List<Eczane>();

            
            foreach (var tr in table.Where(x => x.Count == 4).Take(7).ToList())
            {


                Eczane ecz = new Eczane();
                ecz.Ilce = tr[0];
                ecz.Ad = tr[1];
                ecz.Adres = tr[2];
                ecz.Telefon = tr[3];
                datas.Add(ecz);







            }


            
            return View(datas);
        }



    }
}