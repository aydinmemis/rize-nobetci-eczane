﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HtmlParse.Models
{
    public class Eczane
    {
        public int Id { get; set; }
        public string Ilce { get; set; }
        public string Ad { get; set; }
        public string Adres  { get; set; }
        public string Telefon { get; set; }
    }
}