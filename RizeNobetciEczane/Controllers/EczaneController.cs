﻿using DAL;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;

namespace RizeNobetciEczane.Controllers
{
    public class EczaneController : ApiController
    {
        private string html;
        private string ilce = string.Empty;
        public List<Eczane> cacheDatas;

        // GET: api/Eczane
        public List<Eczane> Get()
        {
            List<Eczane> model = NobetciEczaneGetir(ilce);

            return model;
            //return new string[] { "value1", "value2" };
        }

        #region NobetciEczanegetir()
        private List<Eczane> NobetciEczaneGetir()
        {
            List<Eczane> datas;
            var url = "http://www.rsm.gov.tr/nobetcieczaneler.html";
            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;
            try
            {
                //client ile çağırdığımız sayfayı download edip stringe çeviriyoruz 
                html = client.DownloadString(url);
            }
            catch (Exception)
            {

                throw;
            }

            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(html);

            var table = doc.DocumentNode
           .Descendants("tr")
           .Select(n => n.Elements("td").Select(e => e.InnerText).ToList());
            datas = new List<Eczane>();

            var list = table.Where(x => x.Count == 4).Take(7).ToList();

            foreach (var tr in list)
            {


                Eczane ecz = new Eczane();
                ecz.Ilce = tr[0];
                ecz.EczaneAd = tr[1];
                ecz.Adres = tr[2];
                ecz.Telefon = tr[3];
                datas.Add(ecz);
            }

            return datas;
        }
        #endregion
        private List<Eczane> NobetciEczaneGetir(string ilce)
        {
            List<Eczane> datas;

            var url = "http://www.rsm.gov.tr/nobetcieczaneler.html";

            WebClient client = new WebClient();

            client.Encoding = Encoding.UTF8;
            try
            {
                //client ile çağırdığımız sayfayı download edip stringe çeviriyoruz 
                html = client.DownloadString(url);
            }
            catch (Exception)
            {

                throw;
            }


            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(html);

            var table = doc.DocumentNode
           .Descendants("tr")
           .Select(n => n.Elements("td").Select(e => e.InnerText).ToList());
            datas = new List<Eczane>();

            var list = table.Where(x => x.Count == 4).Take(7).ToList();



            foreach (var tr in list)
            {


                Eczane ecz = new Eczane();
                ecz.Ilce = tr[0];
                ecz.EczaneAd = tr[1];
                ecz.Adres = tr[2];
                ecz.Telefon = tr[3];
                datas.Add(ecz);


            }
            bool isChange = false;

            cacheDatas = null;//Tekrardan database silinince yenilenmesi için koyduk.

            if (cacheDatas == null) { FillCache(); }
            foreach (Eczane item in datas)
            {
                if (!isDataChange(item))
                {
                    isChange = true;
                }
            }
            if (isChange)
            {
                FillCache();
                using (EczaneContext dbContext = new EczaneContext())
                {
                    dbContext.Database.ExecuteSqlCommand("TRUNCATE TABLE Eczane");
                    dbContext.Eczane.AddRange(((List<Eczane>)datas));
                    dbContext.SaveChanges();
                }

            }
            return datas;
        }


        public void FillCache()
        {
            if (cacheDatas == null)
            {
                cacheDatas = new List<Eczane>();
            }
            else
            {
                cacheDatas.Clear();
            }
            using (EczaneContext dbContext = new EczaneContext())
            {
                cacheDatas = dbContext.Eczane.ToList();

            }
        }

        public bool isDataChange(Eczane data)
        {
            return cacheDatas.Any(cd => cd.EczaneAd == data.EczaneAd);
        }

        [Route("api/Eczane/{q}")]
        // GET: api/Eczane/5
        public List<Eczane> Get(string q)
        {
            using (EczaneContext dbContext = new EczaneContext())
            {
                //List<Eczane> model = new List<Eczane>();
                var model = dbContext.Eczane.Where(x => x.Ilce == q).ToList();
                return model;
            }


        }

        // POST: api/Eczane
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Eczane/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Eczane/5
        public void Delete(int id)
        {
        }
    }
}
